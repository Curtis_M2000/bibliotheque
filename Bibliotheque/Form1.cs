﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bibliotheque
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string titre = textBox1.Text;
            string auteur = textBox2.Text;
            int anpub = int.Parse(textBox3.Text);

            Livre l1 = new Livre(titre, auteur, anpub);
            Livre.info.Add(l1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            string titre = textBox5.Text;
            l1.supprimelivre(titre);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            string titre = textBox8.Text;
            l1.recherchelivre(titre);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            string ancien = textBox6.Text;
            string nouveau = textBox11.Text;

            l1.modifierlivre(ancien, nouveau);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Les Categories et leur numeros. \n 0: Science_fiction \n 1: Romance \n 2: Thriller \n 3: Policier \n 4: Humour \n 5: Drame");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            string titre = textBox9.Text;
            int categorie = int.Parse(textBox10.Text);

            l1.ajoutercategorie(titre, categorie);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            int categorie = int.Parse(textBox4.Text);

            l1.recherchecategorie(categorie);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Livre l1 = new Livre();
            string titre = textBox9.Text;
            int categorie = int.Parse(textBox10.Text);
            l1.supprimecategorie(titre, categorie);
        }
    }




    public class Livre
    {
        private string titre;
        private string auteur;
        private int anpub;

        public static int CAT_SCIENCE_FICTION = 0;
        public static int CAT_ROMANCE = 1;
        public static int CAT_THRILLER = 2;
        public static int CAT_POLICIER = 3;
        public static int CAT_HUMOUR = 4;
        public static int CAT_DRAME = 5;

        public static String TITRE_DEFAUT = "Titre inconnu";
        public static String AUTEUR_DEFAUT = "Auteur inconnu";
        public static int ANNEE_PUB_DEFAUT = -1;  

        public static int ANNEE_PUB_MIN = 1900;
        public static int ANNEE_PUB_MAX = 2019;

        public static ArrayList info = new ArrayList(); //tableau contenant les info sur les livres
        public static ArrayList categorie = new ArrayList(); //tableau contenant les categories des livres

        public Livre(string titre, string auteur, int anpub) //constructeur qui ajoute les info sur les livre dans son tableau
        {
            if(titre == "")
            {
                titre = TITRE_DEFAUT;
                MessageBox.Show("Titre inconnue. Titre par defaut applique");
            }

            if(auteur == "")
            {
                auteur = AUTEUR_DEFAUT;
                MessageBox.Show("Auteur inconnue. Auteur par defaut applique");
            }

            if(anpub < ANNEE_PUB_MIN || anpub > ANNEE_PUB_MAX)
            {
                anpub = ANNEE_PUB_DEFAUT;
                MessageBox.Show("Annee inconnue. Annee par defaut applique");
            }

            this.titre = titre;
            this.auteur = auteur;
            this.anpub = anpub;
            MessageBox.Show("Livre bien ajoute");
        }

        public Livre() { } //constructeur vide

        public static bool titrevalide(string titre) //verifie si le titre existe
        {
            int compt = 0;
            foreach (Livre obj in info)
            {
                if (obj.titre == titre)
                { compt++; }
            }
            if (compt == 0) { return false; }

            else { return true; }
        }

        public void recherchelivre(string titre) //methode de recherche de livre
        {
            if (titrevalide(titre))
            {
                string message = "Info sur le livre \n\n";
                foreach (Livre obj in info)
                {
                    if (obj.titre == titre)
                    {
                        message += " Titre : " + obj.titre + "\n Auteur : " + obj.auteur + "\n Annee de publication : " + obj.anpub;
                    }
                }

                message += "\n\nCategories du livre\n";

                foreach (objcat obj in categorie)
                {
                    if (obj.titreobj == titre)
                    {
                        message += " " + obj.catobj + ",";
                    }
                }

                MessageBox.Show(message);
            }
            else
            {
                MessageBox.Show("Titre invalide");
            }
        }

        public void modifierlivre(string titre, string nouveautitre) //methode pour modifier un livre
        {
            if (titrevalide(titre))
            {
                foreach (Livre obj in info)
                {
                    if (obj.titre == titre)
                    {
                        obj.titre = nouveautitre;
                        MessageBox.Show("Titre bien modifie");
                    }
                }
            }
            else
            {
                MessageBox.Show("Titre invalide. Aucune modification effectue");
            }
        }

        public void ajoutercategorie(string titre, int cat) //methode pour ajouter une categorie
        {
            objcat obj = new objcat(titre, cat);
            if(cat < 0 || cat > 5)
            {
                MessageBox.Show("Categorie non-existante");
            }
            else
            {
                if(titrevalide(titre))
                {
                    categorie.Add(obj);
                    MessageBox.Show("Categorie bien ajouté");
                }
                else
                {
                    MessageBox.Show("Titre inconnue");
                }
            } 
        }

        public void recherchecategorie(int cat) //methode pour rechercher les livre par categorie
        {
            string resultat = "Titre de livre dans cette categorie \n";

            if (cat < 0 || cat > 5)
            {
                MessageBox.Show("Categorie non-existante");
            }
            else
            {
                foreach (objcat obj in categorie)
                {
                    if (obj.catobj == cat)
                    {
                        resultat += "-" + obj.titreobj + "\n";
                    }
                }
                MessageBox.Show(resultat);
            }
        }

        public void supprimelivre(string titre) //methode pour supprimer un livre
        {
            Livre test = new Livre();
            objcat test2 = new objcat();
            int compt = -1;

            if (titrevalide(titre))
            {
                foreach (Livre obj in info) //supprime le livre du tableau de livre
                {
                    if (obj.titre == titre)
                    {      
                        test = obj;
                    }
                }
                info.Remove(test);

                foreach (objcat obj in categorie) //supprime le livre du tableau de categorie
                {
                    compt = 0;
                    if (obj.titreobj == titre)
                    {
                        test2 = obj;
                    }
                }
                categorie.Remove(test2);

                MessageBox.Show("Livre bien retiré");
            }
            else
            {
                MessageBox.Show("Titre invalide");
            }
        }

        public void supprimecategorie(string titre, int cat)
        {
            objcat test = new objcat();
            if (titrevalide(titre) && (cat >= 0 && cat <= 5))
            {
                    foreach (objcat obj in categorie)
                    {
                        if (obj.titreobj == titre && obj.catobj == cat)
                        {
                            test = obj;
                        }
                    }
                categorie.Remove(test);
                MessageBox.Show("Categorie bien Retire");
            }
            else
            {
                MessageBox.Show("Titre ou categorie Invalide");
            }
        }

    }



    public class objcat //class pour objet de categorie
    {
        public string titreobj;
        public int catobj;

        public objcat(string titre, int cat)
        {
            this.titreobj = titre;
            this.catobj = cat;
        }

        public objcat() { }
    }
}
